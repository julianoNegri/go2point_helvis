#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Twist.h>
#include <cmath.h>

geometry_msgs::Pose2D _goal,_pose;


ros::Publisher Twist;


void posecb(const geometry_msgs::PoseStamped &p)
{
    double x = p.pose.position.x ;
    double y = p.pose.position.y ;
    double theta = getYaw( p.pose.orientation);

    _pose.x = x;
    _pose.y = y;
    _pose.theta = theta;

    ROS_INFO_STREAM( "Posição: ("<< _pose.x <<","<<_pose.y<<"," << _pose.theta<< ")");
}

void goalcb(const geometry_msgs::PoseStamped &g)
{

    double x = g.pose.position.x ;
    double y = g.pose.position.y ;
    double theta = getYaw( g.pose.orientation);

    _goal.x = x;
    _goal.y = y;
    _goal.theta = theta;

    go2point();

    ROS_INFO_STREAM( "Goal: ("<< _goal.x <<","<<_goal.y<<"," << _goal.theta<< ")");
}

int main( int argc, char **argv)
{
    ros::init(argc,argv, "Kinect_Control");

    ros::NodeHandle node;

    /*  To get the constants using parameters
     * node.getParam( "Rl", Rl);
     * node.getParam( "Rr", Rr);
     * node.getParam( "B", B);
     * node.param( "Red", Red, 1);
     */

    twist_pub = node.advertise<geometry_msgs::Twist>("/xforklift/twist" , 1);

    ros::Subscriber pose = node.subscribe("/camera/pose",1, posecb);
    ros::Subscriber goal = node.subscribe("move_base_simple/goal",1, goalcb);
    ros::spin();
}

void go2point()
{
    geometry_msgs::Twist vel;
    double dx = _goal.x - _pose.x;
    double dy = _goal.y - _pose.y ;

    double theta = atan2( dy,dx );

    double dist = sqrt( dx*dx + dy*dy) ;

    vel.linear.x = VEL * dist ;
    vel.linear.y = 0;
    vel.linear.z = 0;

    vel.angular.x = 0;
    vel.angular.y = 0;
    vel.angular.z =  theta - _pose.theta ;

    twist_pub.publish(vel);

     ROS_INFO_STREAM( " DIST: "<< dist );
}


